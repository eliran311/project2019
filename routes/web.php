<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['verify' => true]);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('tasks', 'TaskController')->middleware('auth');
Route::resource('topics', 'TopicController')->middleware('auth');
Route::resource('meets', 'MeetController')->middleware('auth');
Route::resource('usercodes', 'UsercodeController')->middleware('auth');

Route::resource('users', 'UserController')->middleware('auth');
Route::resource('usermeets', 'UsermeetController')->middleware('auth');
Route::resource('organizations', 'OrganizationController')->middleware('auth');
Route::get('tasks/done1/{id}', 'MeetController@statusmeet')->name('done1');
Route::get('tasks/iscom/{id}', 'MeetController@iscom1')->name('iscom');
Route::get('tasks/trying/{id}', 'MeetController@status')->name('trying');
Route::get('tasks/done/{id}', 'TaskController@done')->name('done');
Route::get('meets/create1/{id}', 'MeetController@create1')->name('create1');
Route::get('users/change', 'UserController@change')->name('change');
Route::get('tasks/create/{meet_id?}', 'TaskController@create');
Route::get('topics/create/{meet_id?}', 'TopicController@create');
Route::get('topics/edit/{meet_id?}', 'TopicController@edit');
Route::get('/sendemail', 'SendEmailController@index');
Route::post('/sendemail/send', 'SendEmailController@send');
Route::get('topics/done/{id}', 'TopicController@done')->name('donetopic');
Route::get('usermeets/create/{meet_id?}', 'UsermeetController@create');
Route::get('/allmeets', 'MeetController@allmeets');

Route::get('/dashboard', 'UserController@dashboard')->name('dashboard');
Route::get('users/code/{id}', 'UserController@code')->name('code');
Route::get('chart', 'ChartController@index');
Route::resource('deshbords', 'DeshbordController')->middleware('auth');