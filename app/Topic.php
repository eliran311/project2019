<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = [
        'name', 'meet_id','namemeet','iscomplete'
    ];
    public function meets()
    {
        return $this->belongsTo('App\Meet');
    }
}
