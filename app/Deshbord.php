<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deshbord extends Model
{
    protected $table = "deshbords";
    protected $fillable = [
        'meet_id', 'count_topic', 'count_tasks', 'task_late','taskcompl','title',
    ];
    public function meets()
    {
        return $this->hasMany('App\Meet');
    }
}
