<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use Illuminate\Support\Facades\Auth;
use Charts;
use DB;
use Illuminate\Support\Facades\Gate;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      if (Gate::denies('manager')){  
        if (Gate::denies('invitor')) {
            if (Gate::denies('participant')) {
            abort(403,"Are you a hacker or what?");} }}  
      $today = date("Y-m-d");
        $id=Auth::id();
        $use=DB::table('users')->where('id',$id)->first();
        if (Gate::allows('manager')){
          $users = User::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
          ->where('users.code', '=' ,$use->code)->get();
          $taskdead = DB::table('tasks')
          ->join('meets', 'tasks.meet_id', '=', 'meets.id') 
         ->where('tasks.user_excute', '=' ,$id)->where('tasks.deadline', '<' ,$today)->where('tasks.iscomplete', '=' ,'0')->get();
          }
          if (Gate::allows('participant')){
            $today = date("Y-m-d");
            $taskdead = DB::table('tasks')
          ->join('meets', 'tasks.meet_id', '=', 'meets.id') 
         ->where('tasks.user_excute', '=' ,$id)->where('tasks.deadline', '<' ,$today)->where('tasks.iscomplete', '=' ,'0')->get();
          }
          if (Gate::allows('invitor')){
            $today = date("Y-m-d");
            $taskdead = DB::table('tasks')
          ->join('meets', 'tasks.meet_id', '=', 'meets.id') 
         ->where('tasks.user_id', '=' ,$id)->where('tasks.deadline', '<' ,$today)->where('tasks.iscomplete', '=' ,'0')->get();
          }
        if (Gate::denies('invitor')) {
          $meets = DB::table('meets')
          ->join('usermeets', 'usermeets.meet_id', '=', 'meets.id') 
          ->select(DB::raw("(count(meets.id))as total"))
         ->where('meets.user_id', '=' ,$id)->orWhere('usermeets.user_id', '=' ,$id)->get();}
          $meets = DB::table('meets')
          ->select(DB::raw("(count(meets.id))as total"))
          ->where('meets.user_id', '=' ,$id)->get();
        $meet=$meets->pluck('total');
    if (Gate::allows('participant') || Gate::allows('manager')) {
        $taskcom = DB::table('tasks')
        ->join('meets', 'tasks.meet_id', '=', 'meets.id') 
       ->where('tasks.user_excute', '=' ,$id)->where('tasks.iscomplete', '=' ,'0')->get();
    }
    if (Gate::allows('invitor')) {
        $taskcom = DB::table('tasks')
        ->join('meets', 'tasks.meet_id', '=', 'meets.id') 
       ->where('tasks.user_id', '=' ,$id)->where('tasks.iscomplete', '=' ,'0')->get();
    }
    if (Gate::allows('manager')){
        $chart = Charts::database($users, 'bar', 'highcharts')
      ->title("Monthly new Register Users")
      ->elementLabel("Total Users")
      ->Width(0)
      ->responsive(true)
      ->groupByMonth(date('Y'), true);
      $id=Auth::id();
    }
      $tasks = DB::table('tasks')
      ->join('meets', 'tasks.meet_id', '=', 'meets.id') 
       ->where('tasks.user_excute', '=' ,$id) ->orWhere('tasks.user_id', '=' ,$id) ->get();
      $bar_chart = Charts::database($tasks, 'bar', 'material')
        ->title('NumOfTasks per meet')
        ->elementLabel("Total Tasks")
        ->Width(0)
        ->responsive(true)
        ->Colors(['#4caf50'])
        ->groupBy('name_meet');
        $pie_meet = Charts::Create('bar', 'highcharts')
      ->title("Total meetings")
      ->elementLabel("Num Of Meets")
      ->Width(0)
      ->labels(['total meets'])
	  ->values([ $meet ])
      ->responsive(true);
  
      $task_com = Charts::database($taskcom, 'bar', 'material')
        ->title(' Tasks per meetings uncomplete')
        ->elementLabel("num")
        ->Width(0)
        ->responsive(true)
        ->Colors(['blue'])
        ->groupBy('name_meet');
        $deadline_com = Charts::database( $taskdead, 'bar', 'material')
        ->title(' Tasks per meetings late')
        ->elementLabel("num")
        ->Width(0)
        ->responsive(true)
        ->Colors(['blue'])
        ->groupBy('name_meet');
          if (Gate::denies('manager')){
        return view('charts.index',compact('deadline_com','bar_chart','pie_meet','task_com'));}
        return view('charts.index',compact('chart','bar_chart','pie_meet','task_com','deadline_com'));
      
    }
}
