<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Task;
use App\User;
use App\Meet;
use App\Usermeet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    
    public function index()
    {
        if (Gate::denies('manager')){  
            if (Gate::denies('invitor')) {
                if (Gate::denies('participant')) {
                abort(403,"Are you a hacker or what?");} }}  
        $id = Auth::id();
        
        if (Gate::allows('invitor')) {
            
        $tasks = Task::where('user_id' , $id)->get();
        return view('tasks.index', compact('tasks'));
        }
       // $task = Usermeet::where('user_id' , $id)->get('meet_id');
       $tasks = Task::where('user_excute' , $id)->get();
     //  $tasks = Task::find($taskss);
       
        return view('tasks.index', compact('tasks'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $meet_id=null )
    {
        $meets=null;
        if(!$meet_id){
            $meets = Meet::where('user_id' , Auth::user()->id)->get();
            $users = User::where('code' , Auth::user()->code)->get();
        }
        return view('tasks.create',['meet_id'=>$meet_id,'meets'=>$meets],['users'=>$users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        
        $task = new Task();
        $id =Auth::id();
       
        $task->name = $request->name;
        $task->user_id = $id;
       
        $task->deadline = $request->deadline;
        $task->meet_id = $request->meet_id ;
        $task->user_excute = $request->user_excute ;
        $task->iscomplete = 0;
        $boss = DB::table('meets')->where('id',$request->meet_id)->first();
        $todayDate = date("Y-m-d");
        $todayTime = date("H:i:s");
        if (($todayDate< $boss->start_date)|| ($todayTime< $boss->start_time) ||  ($todayTime> $boss->end_time) ) {
            abort(403,"sorry you can edit only the meeting");
        }
       $task->name_meet=$boss->title;
        $task->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        
        
        return view('tasks.edit',['task' => $task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function done($id)
    {
        //only if this todo belongs to user 
                 
        $task = Task::findOrFail($id);            
        $task->iscomplete = 1; 
        $task->save();
        return redirect('tasks');    
    }    
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
   
      
        $task->update($request->all()); 
    
        // mark the task as complete and save it
     
         $task->save();

        // flash a success message to the session
        

        // redirect to tasks index
        return redirect('tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
       
        $task->delete();
        return redirect('tasks');
    }
  

}
