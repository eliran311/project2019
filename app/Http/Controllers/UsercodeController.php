<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meet;
use App\Usercode;
use App\User;
use App\Task;
use App\Topic;
use App\Organization;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
class UsercodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($organization_id=null  )
    {
        $id =Auth::id();
        $aa= DB::table('organizations')->where('id',$id)->first();
        if ( Organization::where('org_name',$aa->org_name)->count() == '1') {
            $use=  DB::table('users')->where('id', $id) ->update(
                ['role' => 'manager']
                
            );}
        
        
       else {
        $u=DB::table('users')->where('id', $id) ->update(
            ['role' => 'participant']
        );}
          
        $organizations=null;
        if(!$organization_id){
         //   $organizations =  DB::table('organizations')->select('org_name')->groupBy('org_name') ->get();            
         $organizations= Organization::all()->unique('org_name');
        }
        return view('usercodes.create',['organization_id'=>$organization_id,'organizations'=>$organizations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $usercode = new Usercode();
        $id =Auth::id();
      
       
        
        $usercode->organization_id = $request->organization_id;
       $user =  DB::table('users')->where('id',$id)->first();
       $usercode->user_id=$id;
       if (Gate::allows('manager')) {
           
       $use=  DB::table('users')->where('id', $id) ->update(
        ['code' => $request->organization_id]
        
    );}
       $usercode->save();
       
        return redirect('meets');
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
