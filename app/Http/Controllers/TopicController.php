<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meet;
use App\User;
use App\Task;
use App\Topic;
use App\Usermeet;
use App\Organization;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('manager')){  
            if (Gate::denies('invitor')) {
                if (Gate::denies('participant')) {
                abort(403,"Are you a hacker or what?");} }}  
        $id=Auth::id();
       if (Gate::allows('invitor')) {
            $meets=  DB::table('meets')->where('user_id', $id)->value('id');
            $topics=  Topic::where('meet_id' , $meets)->get();
           return view('topics.index', compact('topics'));
        }
     $meeting = DB::table('usermeets')->where('user_id', $id)->value('meet_id');
     $topics = Topic::where('meet_id' , $meeting)->get();
        return view('topics.index', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $meet_id=null )
    {
        $meets=null;
        if(!$meet_id){
            $meets = Meet::where('user_id' , Auth::user()->id)->get();
        }
        return view('topics.create',['meet_id'=>$meet_id,'meets'=>$meets]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $topic = new Topic();
        $id =Auth::id();
        
        
        $topic->name = $request->name;
       $topic->meet_id=$request->meet_id;
        
        $boss = DB::table('meets')->where('id',$request->meet_id)->first();
       $topic->namemeet=$boss->title;
       $user =  DB::table('users')->where('id',$id)->first();
       $bossorg = DB::table('organizations')->where('id',$user->code)->first();
       $try = $bossorg->min_item;
       $topic->save();
       $organization=Organization::all();
      
        $votes = Topic::where('meet_id',$request->meet_id)->count();
        
        
        if ($votes<$try){
        return redirect('topics/create');}
        return redirect('usermeets/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topic = Topic::find($id);
        $meets = Meet::where('user_id' , Auth::user()->id)->get();
        
        return view('topics.edit',['topic' => $topic,'meets'=>$meets]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $topic = Topic::find($id);
        $topic->update($request->all()); 
        return redirect('topics');
    }
    public function done($id)
    {
        //only if this todo belongs to user 
                 
        $topic = Topic::findOrFail($id);  
        $boss = DB::table('meets')->where('id',$topic->meet_id)->first();        
        $todayDate = date("Y-m-d");
        $todayTime = date("H:i:s");
        if (($todayDate< $boss->start_date)|| ($todayTime< $boss->start_time) ||  ($todayTime> $boss->end_time) ) {
            abort(403,"sorry you can edit only the meeting ");
        }  
        $topic->iscomplete = 1; 
        $topic->save();
        return redirect('topics');    
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::find($id);
        $topic->delete();
        return redirect('topics');

    }


}
