<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Task;
use App\Meet;
use App\User;
use App\Organization;
use App\Usercode;
use App\Usermeet;
use App\Charts\UserChart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (Gate::denies('manager')){  
        if (Gate::denies('invitor')) {
            if (Gate::denies('participant')) {
            abort(403,"Are you a hacker or what?");} }}  
       if (Gate::denies('manager')) {
           abort(403,"Are you a hacker or what?");}
          // $users=User::all();
        $id=Auth::id();
        
       $org =  DB::table('organizations')->where('id',$id)->first();
       $o=$org->id;
       $use =  Usercode::where('organization_id' , $o)->get('user_id');
  
     //  $users =  DB::table('users')->where('id',$use->user_id)->get();
       $users =  User::find($use);
      
    
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(  )
    {
       // $organizations=null;
        //if(!$organization_id){
          //  $organizations = Organization::all();
        //}
        //return view('users.create',['organization_id'=>$organization_id,'organizations'=>$organizations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       // $user = DB::table('organizations')->where('id',$request->organization_id)->first();

      //  $user->code = $request->organization_id;
      
    
    //    return redirect('users');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show(request $request)
    {
       // $result = DB::insertGetId("insert into usercodes (organization_id,user_id) values (?,?,?)",
       // [1,5]);
    
     //   print_r($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id )
    {
        
        $user = User::find($id);
        
        
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}
        $user->update($request -> all());
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}
      if ($user->role != 'manager'){
        $user->delete();}
        return redirect('users');
    }

    public function dashboard()
    {
      $today_users = User::whereDate('created_at', today())->count();
      $yesterday_users = User::whereDate('created_at', today()->subDays(1))->count();
      // $users_2_days_ago = User::whereDate('created_at', today()->subDays(2))->count();
      $users_last_year = User::whereYear('created_at', date('Y'))->count();
      
      if (Gate::allows('manager')) {
      $chart = new UserChart;
      $chart->labels(['Last year', 'Yesterday', 'Today']);
      $chart->dataset('Total users', 'line', [$users_last_year, $yesterday_users, $today_users]);
      
      $chart2 = new UserChart;
      $chart2->labels(['Last year', 'Yesterday', 'Today']);
      $chart2->dataset('Total users', 'bar', [$users_last_year, $yesterday_users, $today_users]);
      

     // $id=Auth::id();
      
     // $meets = Usermeet::where('user_id' , $id)->get('meet_id');
      //$titles =Meet::where('id' , $meets)->get('title');
      //$chart3 = new UserChart;
      
      //$chart3->labels([$titles]);
      ///$chart3->dataset('Total ', 'bar', [$users_2_days_ago]);
      return view('dashboard.index', compact('chart','chart2'));}
      else  return redirect('home');
    }
    
    public function code($id)
    {
        $user = User::findOrFail($id);
        $boss =  DB::table('usercodes')->where('user_id',$id)->first();    
        $user->code=$boss->organization_id;
        $user->save();
        return redirect('users');
    }
}