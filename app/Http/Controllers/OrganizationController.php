<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Task;
use App\Organization;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (Gate::denies('manager')){  
        if (Gate::denies('invitor')) {
            if (Gate::denies('participant')) {
            abort(403,"Are you a hacker or what?");} }}  
        $id=Auth::id();
       //$user = User::find($id);
       
       
     $use =  DB::table('organizations')->pluck('id');
     
      $boss =  DB::table('users')->where('id',$use)->first();
      
        $organizations= Organization::where('id' , $id)->get('organization_id');
        $organizations=  Organization::find($organizations)->unique();
       
            return view('organizations.index', compact('organizations'));
      
    }
  //  public function index()
   // {
     //   $id=Auth::id();
       // $organizations = Organization::all();
         //   return view('organizations.index', compact('organizations'));
      
   // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( )
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organization = Organization::find($id);
        if (Gate::denies('manager')) {
            abort(403,"you can't edit min_item");
        }
        return view('organizations.edit', compact('organization'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $organization = Organization::find($id);
     $organization->update($request->all());           
     return redirect('organizations');
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
