<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meet;
use App\User;
use App\Task;
use App\Topic;
use App\Organization;
use App\Usermeet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
class UsermeetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('manager')){  
            if (Gate::denies('invitor')) {
                if (Gate::denies('participant')) {
                abort(403,"Are you a hacker or what?");} }}  
        $id = Auth::id();
     
        if (Gate::allows('invitor')) {
            $meets=  DB::table('meets')->where('user_id', $id)->value('id');
           
            $usermeets= Usermeet::where('meet_id' , $meets)->get();
        return view('usermeets.index', compact('usermeets'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $meet_id=null )
    {
        $meets=null;
       $id=Auth::id();
        if(!$meet_id){
            $meets = Meet::where('user_id' , Auth::user()->id)->get();
            $usermeets= DB::table('users')->where('id',$id)->first();
           // $user=$usermeets->code;
            $users =  User::where('code' , $usermeets->code)->get();
        }
        return view('usermeets.create',['meet_id'=>$meet_id,'meets'=>$meets],['users'=>$users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usermeet = new Usermeet();
        $id =Auth::id();
        
        
       $usermeet->meet_id = $request->meet_id;
       $usermeet->user_id=$request->user_id;
       $bossmeet = DB::table('meets')->where('id',$request->meet_id)->first();
       $usermeet->namemeet=$bossmeet->title;
       $bossuser = DB::table('users')->where('id',$request->user_id)->first();
       
       $usermeet->nameuser=$bossuser->name;
       $usermeet->save();
      
        return redirect('usermeets');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
