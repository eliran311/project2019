<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Meet;
use App\User;
use App\Task;
use App\Topic;
use App\Usermeet;
use App\Organization;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
class MeetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function statusmeet($id)
    {
        $meet = Meet::findOrFail($id);
        $todayDate = date("Y-m-d");
        $todayTime = date("H:i:s");
        if (($todayDate< $meet->start_date)|| ($todayDate > $meet->start_date)|| ($todayTime< $meet->start_time) ||  ($todayTime> $meet->end_time) ) {
            abort(403,"sorry you can edit only in the meeting");
        }
        
        $meet->statusmeet=1;
        $meet->save();
        return redirect('tasks/create');
    }
    public function iscom1($id)
    {
        $meet = Meet::findOrFail($id);
        $todayDate = date("Y-m-d");
        $todayTime = date("H:i:s");
       if ($meet->statusmeet=1){
        if (($todayDate< $meet->start_date)|| ($todayDate > $meet->start_date)|| ($todayTime< $meet->start_time) ||  ($todayTime> $meet->end_time) ) {
            return redirect('tasks');
        }
        return redirect('tasks/create');
       
    }
}
    public function index()
    {
        if (Gate::denies('manager')){  
            if (Gate::denies('invitor')) {
                if (Gate::denies('participant')) {
                abort(403,"Are you a hacker or what?");} }}  
        $id = Auth::id();
      
        if (Gate::allows('invitor')) {
           
            $meets = User::find($id)->meets;
            if ($meets->user_id=Auth::id() ) 
            return view('meets.index', compact('meets'));
        }
       

        
        
        $meet = Usermeet::where('user_id' , $id)->get('meet_id');
        $meets = Meet::find($meet);
        return view('meets.index', ['meets'=>$meets]);
    }
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('invitor')) {
            abort(403,"Sorry you are not allowed to create meets..");
       }
        
      
        
        return view('meets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    
    
     public function store(Request $request)
    {
        if (Gate::denies('invitor')) {
            abort(403,"Are you a hacker or what?");
        }
        
        $meet = new Meet();
        $id =Auth::id();

        $meet->title = $request->title;
        
        $meet->start_time = $request->start_time;
        $meet->start_date = $request->start_date;
        $meet->end_time = $request->end_time;
        if ($request->start_time>$request->end_time){
            abort(403,'sorry, endtime cant be before starttime');
        }
        
        $meet->user_id = $id;
        $meet->save();
        return redirect('topics/create');
    
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meet = Meet::find($id);
        $todayDate = date("Y-m-d");
        if (Gate::denies('invitor')) {
            abort(403,"Are you a hacker or what?");}
        return view('meets.edit', compact('meet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
   
     public function update(Request $request, $id)
    {
        $meet = Meet::find($id);
       // if (Gate::denies('invitor')) {
           // if ($request->has('title'))
                  // abort(403,"You are not allowed to edit todos..");
       // }   
    
       $meet->update($request->all()); 
    
       
    
        $meet->save();
        
        return redirect('meets');
    

    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meet = Meet::find($id);
        if (Gate::denies('invitor')) {
            abort(403,"Are you a hacker or what?");}
      
        $meet->delete();
        return redirect('meets');
    }

    public function allmeets()
    {
    //    $meets=  Meet::all();
      $id =Auth::id(); 
      $meet = Meet::all();
      if ($meet->isEmpty()){
        return redirect('meets');
     }
     else {
     $use =  DB::table('meets')->pluck('user_id');
     // $use = DB::table('meets')->where('id','8')->first();
      $boss =  DB::table('users')->where('id',$use)->first();
     $currentuse =  DB::table('users')->where('id',$id)->first(); 
      if ($boss->code == $currentuse->code){
        $meets= Meet::where('user_id' , $boss->id)->get();

       return view('meets.index', ['meets'=>$meets]);
    
   
        }
       return redirect('meets');
    }
   
    }
   


}