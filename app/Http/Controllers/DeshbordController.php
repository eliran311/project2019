<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Task;
use App\Deshbord;
use App\Meet;
use App\User;
use App\Organization;
use App\Usercode;
use App\Usermeet;
use App\Topic;
use App\Charts\UserChart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
class DeshbordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('manager')){  
            if (Gate::denies('invitor')) {
                if (Gate::denies('participant')) {
                abort(403,"Are you a hacker or what?");} }}  
        $id=Auth::id();
       // $meeting=  Topic::where('meet_id' , $meets)->get('meet_id');
       if (Gate::denies('invitor')) {
        $meets = DB::table('usermeets')->where('user_id', $id)->value('meet_id');}
        $meets = DB::table('meets')->where('user_id', $id)->value('id'); 

    //$meets = DB::table('meets')->where('user_id', $id)->value('id'); 
        $todayDate = date("Y-m-d");
        //$meetings = Meet::where('id',$meeting)->get();
        $task= DB::table('tasks')->where('meet_id', $meets)->value('deadline');
        $iscom= DB::table('tasks')->where('meet_id', $meets)->value('iscomplete');
        $title= DB::table('meets')->where('id', $meets)->value('title');
        $tasks = Task::where('meet_id' , $meets)->count();
        $topics = Topic::where('meet_id' , $meets)->count();
        $taskcom = Task::where('meet_id' , $meets)->where('iscomplete', '0')->count();
       if ($meets!=NULL){
        if  (Deshbord::where('meet_id' , $meets)->count() == 0){
         if  (($task < $todayDate ) & ($iscom == 0)) {  
            $tasi =  DB::table('tasks')->where('deadline', '<', $todayDate)->where('meet_id', $meets)->value('meet_id');    
       $taskslate = Task::where('meet_id' , $tasi)->count();
        $deshbord = DB::table('deshbords')->insertGetId(
            ['meet_id' => $meets  , 'count_tasks' => $tasks , 'count_topic' => $topics , 'task_late' => $taskslate , 'taskcompl' => $taskcom , 'title'=> $title]
        );}
       else if ($task == $todayDate ) {
            $tasi =  DB::table('tasks')->where('deadline', '=', $todayDate)->where('meet_id', $meets)->value('meet_id');
         $taskslate = Task::where('meet_id' , $tasi)->count();
           $deshbord = DB::table('deshbords')->insertGetId(
            ['meet_id' => $meets  , 'count_tasks' => $tasks , 'count_topic' => $topics , 'task_late' => '0' , 'taskcompl' => $taskcom , 'title'=> $title]
        );}
        else {
            $deshbord = DB::table('deshbords')->insertGetId(
                ['meet_id' => $meets  , 'count_tasks' => $tasks , 'count_topic' => $topics , 'task_late' => 0 , 'taskcompl' => $taskcom , 'title'=> $title]
            );}}
            else {
                if  (($task < $todayDate ) & ($iscom == 0)) { 
                    $tasi =  DB::table('tasks')->where('deadline', '<', $todayDate)->where('meet_id', $meets)->value('meet_id');
                    $taskslate = Task::where('meet_id' , $tasi)->count();
                $use=  DB::table('deshbords')->where('meet_id', $meets) ->update(
                    ['count_topic' => $topics, 'count_tasks' => $tasks, 'task_late'=> $taskslate, 'taskcompl'=>$taskcom , 'title'=> $title]
                    
                );}
                else {
                    $use=  DB::table('deshbords')->where('meet_id', $meets) ->update(
                    ['count_topic' => $topics, 'count_tasks' => $tasks, 'task_late'=> '0', 'taskcompl'=>$taskcom , 'title'=> $title]
                    
                );} 

            }
        $user = DB::table('usermeets')->where('user_id', $id)->value('meet_id');
        $deshbord=Deshbord::where('meet_id' , $user)->get();
        $deshbords=$deshbord->unique('meet_id');}
        else{
            $user = DB::table('usermeets')->where('user_id', $id)->value('meet_id');
            $deshbord=Deshbord::where('meet_id' , $user)->get();
            $deshbords=$deshbord->unique('meet_id');
        }
       if (Gate::allows('invitor')) {
        $user = DB::table('meets')->where('user_id', $id)->value('id');
        $deshbord=Deshbord::where('meet_id' , $user)->get();
        $deshbords=$deshbord->unique('meet_id');
       }     
         
        

      //  $deshbords =  Deshbord::find($use);
    return view('dashbords.index', compact('deshbords'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
