<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usercode extends Model
{
    protected $fillable = [
        'organization_id','user_id'
   ];
}
