<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
        'organization_id', 'org_name', 'manager_name','min_item','id',
    ];
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
