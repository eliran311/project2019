<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meet extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    protected $fillable = [
        'title', 'user_id', 'start_time','start_date','statusmeet','topics'
    ];
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
    public function topics()
    {
        return $this->hasMany('App\Topic');
    }
    
}
