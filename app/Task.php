<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function meets()
    {
        return $this->belongsTo('App\Meet');
    }
    protected $fillable = [
        'name', 'deadline', 'user_id','meet_id','iscomplete','name_meet','user_excute'
    ];
    
}
