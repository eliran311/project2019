<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDashbordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deshbords', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('meet_id');
            $table->string('count_topic');
            $table->string('count_tasks');
            $table->string('task_late')->nullable();
            $table->string('taskcompl')->nullable();
            $table->string('title')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashbords');
    }
}
