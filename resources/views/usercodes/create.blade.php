@extends('layouts.app')
@section('content')



<h1>submit your organization</h1>

<form method = 'post' action = "{{action('UsercodeController@store')}}" >

{{csrf_field()}}      
 
<div class = "form-group2">
    
  
    @if($organizations != null)
    <div class="col-md-4">
    <label class="control-label" for = "name"> Organization Name </label>
    </div>

    <div class="col-md-10">
    <select class="form-control" name = "organization_id">
    @foreach($organizations as $organization)
  
  <option value="{{$organization->id}}"> {{$organization->org_name}}</option>
  @endforeach  
  </select></div>
  

@endif




    
<br>
<div class = "form-group">    
    <input type = "submit" class = "submit-button" name = "submit" value = "Save">

</div>

</form>

@endsection


<style>
.submit-button{
  background-color: #555555; 
  border: none;
  color: white;
  padding: 8px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  height: 40px;
}

.form-group2{
  background: #E6E6FA;
    width: 60%;
    border-radius: 3%;
    padding: 2%;

}

.py-4{
  padding-left:30px;
} 

.form-group{
padding-left:15px;

}



</style>