@extends('layouts.sidebar')
@section('content')

<h1>Create a new org</h1>
<form method = 'post' action = "{{action('UserController@store')}}" >
{{csrf_field()}}      
 

    @if($organizations != null)
    <div class="col-md-2">
    <label class="control-label" for = "name"> org Name </label>
    </div>

    <div class="col-md-10">
    <select class="form-control" name = "organization_id">
    @foreach($organizations as $organization)
  
  <option value="{{$organization->id}}"> {{$organization->org_name}}</option>
  @endforeach  

  
</select></div>@endif

  


<div class = "form-group">  
<div class="col-md-2">  
    <input type = "submit" class = "form-control" name = "submit" value = "Save">
</div></div>

</form>

@endsection
