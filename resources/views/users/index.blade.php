@extends('layouts.sidebar')

@section('content')
<div class="w3-sidebar w3-bar-block " style="margin: 0 0 5% 0;" >
                
                <div class="sidebar-module">
                <h4>Users</h4>
                <ol class="list-unstyled">
                    
                    <!-- <li><a href="{{route('meets.index')}}" class="btn btn-success">@lang('Back to meets')</a></li> -->
                       
                </ol>
                </div>
     
    

      
 </div>
 <div class="form-group"> 
			 		<select class  ="form-control" name="state" id="maxRows">
						 <option value="5000">Show ALL Rows</option>
						 <option value="5">5</option>
						 <option value="10">10</option>
						 <option value="15">15</option>
						 <option value="20">20</option>
						 <option value="50">50</option>
						 <option value="70">70</option>
						 <option value="100">100</option>
						</select>
			 		
			  	</div>
    <table >   

<thead><tr>
<th > Name</th>
<th > Email</th>
<th > Role</th>
<th > code</th>
</tr> </thead>
<tbody>@foreach($users as $user)
<td > {{$user->name}}</td>
<td > {{$user->email}}</td>
<td > {{$user->role}}</td>
@if ($user->code == 0)
<td>  <a href="{{route('code', $user->id)}}">@lang('submit')</a> </td>
    @else
    <td > {{$user->code}}</td>
@endif

<td><a href="{{route('users.edit', $user->id)}}" class="btn btn-success">@lang('Edit User Role')</a></td>
<td> 
<form method = 'post' action = "{{action('UserController@destroy',$user->id)}}" >
@csrf   
@method('DELETE')   
<div style="margin:0">    
<button type="submit" class="btn btn-danger">@lang('delete')</button>
</div>
</form></td> 
</tr>
 @endforeach
</tbody>
</table>
</div>
@endsection
<style>
.ttt{
   position:absolute;
   bottom:0;
   width:100%;
   height:30px;   /* Height of the footer */
   background:#6cf;
   text-align:center;
}
.rr{
    background-color: green;
}
.rrr{
    background-color: green;
}
table,th,td{
     border: 1px solid black;
     height: 10vh;
      margin: 0;
      
}
.el{
   
    margin-bottom: 10px;
}
.try{
    padding: 0 25px ! important;
    font-size: 50px ! important;
}
th,td{
    padding: 0 25px ! important;
}
.r{
    font-weight:bold ;
}

</style>

