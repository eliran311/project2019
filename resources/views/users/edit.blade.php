@extends('layouts.sidebar')
@section('content')

<form method="post"  action="{{action('UserController@update', $user->id) }}">
 @method('PATCH')
 @csrf
 
 <div class="form-group">
   <label for="role">Update role</label>
   <!--<input type="text" class="form-control" name="role" value="{{ $user->role}}" />-->
   
  
  <select class="form-control" name="role" id="role" value="{{ $user->role}}" >
    <option>manager</option>
    <option>invitor</option>
    <option>participant</option>
  
  </select>
   
 
 
 
</div>
 <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection