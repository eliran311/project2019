@extends('layouts.sidebar')

@section('content')

  
<div class="panel panel-default panel-table">
     <div class="row">
             <div class="col col-xs-6" style="font-family: Arial Black, Gadget, sans-serif;">
                <h3>Organization</h3>
             </div>
             </div>
                </div>           
               
                    
                    <!-- <li><a href="{{route('meets.index')}}" class="btn btn-success">@lang('Back to meets')</a></li> -->
                    
 
<div class="table-responsive" style="width:70%;padding: 5% 0 5% 1%;">
    <table class="table table-bordered"> 

<thead><tr>
<th style=" text-align: center;" > Organization Name</th>
<th  style=" text-align: center;"> Item for Meeting</th>
</tr>
 </thead>
<tbody>
    @foreach($organizations as $organization)
    
<tr>

<td > {{$organization->org_name}}</td>
<td> {{$organization->min_item}} </td>

@can('manager')<td><a href="{{route('organizations.edit', $organization->id)}}" class="btn btn-success">@lang('Edit')</a></td>
@endcan
</tr>


  
@endforeach  
</tbody>
</table>
</div>
</div>


                
        
 <!-- <footer class="ttt">Website of EISS</footer> -->

    
   
@endsection

