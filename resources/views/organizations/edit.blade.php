@extends('layouts.sidebar')
@section('content')

<h1>edit a meet</h1>
<form method = 'post' action = "{{action('OrganizationController@update', $organization->id)}}"  >
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "item"> Item to update </label>
    <input type = "text" class = "form-control" name = "min_item" value = "{{$organization->min_item}}">
  
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Update">
</div>



@endsection
