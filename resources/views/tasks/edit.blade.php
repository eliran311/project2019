@extends('layouts.sidebar')

@section('content')
<h1>Edit task</h1>
<form method="post"  action="{{action('TaskController@update', $task->id) }}">
 @method('PATCH')
 @csrf
 <div class="form-group" style="    background: #E6E6FA;
    width: 50%;
    border-radius: 3%;
    padding: 2%;">
 <div class="col-md-10">  
   <label for="name">Update name</label>
   </div>
   <div class="col-md-10">
   <input type="text" class="form-control" name="name" value="{{ $task->name}}" />
   </div>
   <div class="col-md-10">  
   <label for="deadline">Update deadline</label>
   </div>
   <div class="col-md-10">
   <input type="text" class="form-control" name="deadline" value="{{ $task->deadline}}" />
   </div>
   <button type="submit" class="btn btn-primary" style="margin-top:1rem">Update</button>
 </div>
 
</form>
@endsection