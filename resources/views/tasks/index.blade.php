@extends('layouts.sidebar')

@section('content')

  
<div class="w3-sidebar w3-bar-block " style="margin: 0 0 5% 0;" >
                
                <div class="sidebar-module" style="font-family: Arial Black, Gadget, sans-serif;">
                <h4>Tasks</h4>
                <ol class="list-unstyled">
                    
                    <!-- <li><a href="{{route('meets.index')}}" class="btn btn-success">@lang('Back to meets')</a></li> -->
                    
                </ol>
                </div>
               <!-- <div class="col">   
             <a href="{{ route('tasks.create') }}"
                  class="btn btn-sm btn-primary btn-create">
                     @lang('add a new task')</a>   
       </div> -->          

 </div>
 

 <div class="table-responsive" >
    <table class="table table-bordered"> 

<thead>
<tr>
<th style = "vertical-align: middle;text-align: center;border-right: 1px solid #dee2e6;"> Name</th>
<th style = "vertical-align: middle;text-align: center;border-right: 1px solid #dee2e6;"> DeadLine</th>
<th style = "vertical-align: middle;text-align: center;border-right: 1px solid #dee2e6;"> Name of the meet</th>
</tr>
 </thead>

<tbody>

    @foreach($tasks as $task)

<tr>
<td align="center" style="vertical-align: middle;"> {{$task->name}}</td>
    @if (date('Y-m-d') == $task->deadline)
         <td align="center" style="vertical-align: middle;"> {{$task->deadline}} </td>

    @elseif((date('Y-m-d') > $task->deadline) & ($task->iscomplete == 0))
         <td  align="center" style="vertical-align: middle;"><font color="red"><b> Task was late: {{$task->deadline}} </b> </font></td> 
   @else
   <td align="center" style="vertical-align: middle;"> {{$task->deadline}} </td>
   @endif
<td align="center" style="vertical-align: middle;"> {{$task->name_meet}} </td>

@cannot('participant')
<td align="center" style="vertical-align: middle;"><a href="{{route('tasks.edit', $task->id)}}" class="btn btn-success">@lang('Edit')</a></td>
<td align="center" style="vertical-align: middle;"> 
<form method = 'post' action = "{{action('TaskController@destroy',$task->id)}}" style="margin:0">
@csrf   
@method('DELETE')    
<div style="margin:0">    
<button type="submit" class="btn btn-danger">@lang('delete')</button>
</div>@endcannot
</form>
</td> 

@if (($task->iscomplete == 0) & (date('Y-m-d H:i:s') < $task->deadline))        
<td align="center" style="vertical-align: middle;">    <a href="{{route('done', $task->id)}}">Mark As done</a> </td>
             @elseif  (($task->iscomplete == 0) & (date('Y-m-d') == $task->deadline))
            <td align="center" style="vertical-align: middle;"> <a href="{{route('done', $task->id)}}">Mark As done</a> </td>
            @elseif (($task->iscomplete == 0) & (date('Y-m-d H:i:s') > $task->deadline))
            
            @else
            Done!
            @endif
                

</tr>
 



<br>

  
@endforeach  
</tbody>
</table>
</div>





                

 <!-- <footer class="ttt">Website of EISS</footer> -->

    
       
@endsection
<style>

 .col {
    position: absolute;
    top: 40px;
    left: 805px;
  }

.ttt{
   position:absolute;
   bottom:0;
   width:100%;
   height:30px;   /* Height of the footer */
   background:#6cf;
   text-align:center;
}

.rr{
    background-color: green;
}
.rrr{
    background-color: green;
}
table,th,td{
     
     height: 10vh;
      margin: 0;
      
}
.el{
   
    margin-bottom: 10px;
}
.try{
    padding: 0 25px ! important;
    font-size: 50px ! important;
}
th,td{
    padding: 7px 25px ! important;
}
.r{
    font-weight:bold ;
}



</style>