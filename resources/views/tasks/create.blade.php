@extends('layouts.sidebar')
@section('content')

<h1>Create a new task</h1>
<form method = 'post' action = "{{action('TaskController@store')}}" >
{{csrf_field()}}      
 
<div class = " form-group2">
<div class="col-md-4">
    <label class="control-label" for = "name"> Task Name </label>
    </div>
    <div class="col-md-10">
    <input type = "text" class = "form-control" name = "name" required>
    </div>
    <div class="col-md-4">
    <label class="control-label" for="date"> Task Deadline </label>
    </div>
    <div class="col-md-10">
    <input type = "date" name="deadline" id="date" class="form-control" value="{{ date("Y-m-d") }}" required />
    </div>
    @if($meets != null)
    <div class="col-md-4">
    <label class="control-label" for = "name"> Meet Name </label>
    </div>

    <div class="col-md-10">
    <select class="form-control" name = "meet_id">
    @foreach($meets as $meet)
  
  <option value="{{$meet->id}}"> {{$meet->title}}</option>
  @endforeach  

  
</select></div>@endif
<div class="col-md-4">
    <label class="control-label" for = "name"> user Name </label>
    </div>

    <div class="col-md-10">
    <select class="form-control" name = "user_excute">
    @foreach($users as $user)
  
  <option value="{{$user->id}}"> {{$user->name}}</option>
  @endforeach  
  
</select>
</div>
<br>
<div class = "form-group">  
<div class="col-md-4">  
    <input type = "submit" class = "submit-button" name = "submit" value = "Save">
</div>
</div>
</div>
  




</form>

@endsection

<style>

.submit-button{
  background-color: #555555; 
  border: none;
  color: white;
  padding: 8px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  height: 40px;
}

 .form-group2{
  background: #E6E6FA;
    width: 60%;
    border-radius: 3%;
    padding: 2%;

}

.py-4{
  padding-left:30px;
} 

</style>