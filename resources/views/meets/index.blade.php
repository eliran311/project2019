

@extends('layouts.sidebar')
@section('content')
    
    
<div class="w3-sidebar w3-bar-block " style="margin: 0 0 5% 0;" >
         
         <div class="sidebar-module" style="font-family: Arial Black, Gadget, sans-serif;">
           <h2>Meetings</h2>
           <ol class="list-unstyled">
             <!-- <li>
               <a href="{{route('tasks.index')}}" class="w3-bar-item w3-button">Tasks
               </a>
              </li> -->
           
             
          </ol>
        
       </div>
      @can('invitor') <div class="col">   
             <a href="{{ route('meets.create') }}"
                  class="btn btn-sm btn-primary btn-create">
                     @lang('add a new meeting')</a>   
       </div>  @endcan('invitor')          
 </div>

 <div class="panel panel-default">
<div class="panel-heading">
@if (Request::is('meets')) 
<h3 class="panel-title" >My meets</h3>
@endif

</div>
<br>
<table class="table">
<thead>
<tr>
<th > Name</th>
<th > Start Time</th>
<th > End Time</th>
<th > Start date</th>
</tr>
</thead>

<tbody>
@foreach($meets as $meet)
  <tr>
 
<td >{{$meet->title}} </td>
<td > {{$meet->start_time}}</td>
<td > {{$meet->end_time}}</td>
<td > {{$meet->start_date}}</td>


@cannot('participant')<td style="display: flex;
    justify-content: space-around;">   
<a href = "{{route('meets.edit',$meet->id)}}"  class="btn btn-success">@lang('Edit meet')</a>
@if ($meet->statusmeet == 0)
    <a href="{{route('done1', $meet->id)}}" class="btn btn-secondary" > @lang('Edit tasks')</a> 
    @else
<a href = "{{route('iscom', $meet->id)}}" class="btn btn-success">@lang('Edit tasks')</a>
@endif
</td>

<td> 

<form method = 'post' action = "{{action('MeetController@destroy',$meet->id)}}" >
@csrf   


@method('DELETE')    
<div class = "form-group">    
<button type="submit" class="btn btn-danger">@lang('delete')</button>
</div>@endcannot
</form>
</td> 
</tr>


@endforeach
</tbody>
</table>

</div>

</div>


 @can('manager')
 @if (Request::is('meets')) 
<h2><a href="{{action('MeetController@allmeets')}}">show all meets</a></h2>    
@else
<h2><a href="{{action('MeetController@index')}}">show My meets</a></h2>
@endif
@endcan
<br>
<!-- @if (Request::is('meets')) 
<h2>My meets</a></h2>
@endif -->

<!-- <div class="panel-body table-responsive"> -->
    
    <!-- <table class= "table-bordered table-striped">
<thead>
<tr>

<th > Name</th>
<th > Start Time</th>
<th > End Time</th>
<th > Start date</th>

</tr> 
</thead>
<tbody>
  @foreach($meets as $meet)
  <tr>
 
<td ><a href = "{{route('create1', $meet->id)}}" >{{$meet->title}} </a></td>
<td > {{$meet->start_time}}</td>
<td > {{$meet->end_time}}</td>
<td > {{$meet->start_date}}</td>


<td>   
<a href = "{{route('meets.edit',$meet->id)}}"  class="btn btn-success">@lang('Edit meet')</a>
@if ($meet->statusmeet == 0)
    <a href="{{route('done1', $meet->id)}}">@lang('Edit tasks')</a> 
    @else
<a href = "{{route('tasks.index', $meet->id)}}" class="btn btn-success">@lang('Edit tasks')</a>
@endif
</td>

<td> 

<form method = 'post' action = "{{action('MeetController@destroy',$meet->id)}}" >
@csrf   


@method('DELETE')    
<div class = "form-group">    
<button type="submit" class="btn btn-danger">@lang('delete')</button>
</div>
</form>
</td> 
</tr>


@endforeach
</tbody>
</table> -->
<!-- </div> -->




    

 
 @endsection
<!-- <footer class="ttt">Website of EISS</footer> -->

<style>


  .col {
    position: absolute;
    top: 87px;
    left: 750px;
  }

.ttt{
   position:absolute;
   bottom:0;
   width:100%;
   height:30px;   
   background:#6cf;
   text-align:center;
}


</style>

