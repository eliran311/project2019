@extends('layouts.sidebar')
@section('content')
<h1>Create a new meet</h1>
<form method = 'post' action = "{{action('MeetController@store')}}" >
{{csrf_field()}}      

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  

<div class = "form-group">
<div class="col-md-2">
    <label class="control-label" for = "title"> meet name </label></div>
    <div class="col-md-10">
     <input type = "text" class = "form-control" name = "title" required>
     </div>
    <div class="col-md-2">
    <label class="control-label" for="date">start date </label></div>
    
    <div class="col-md-10">
    <input type = "date" name="start_date" id="date" class="form-control" value="{{ date("Y-m-d") }}" required />
    </div>

    <div class="col-md-2">
    <label class="control-label" for="time">Start Time: </label>   </div>
   
   <div class="col-md-10">
    <input type="time" name="start_time" id="time" class="form-control" value="{{ date("H:i") }}" required /></div>
    <div class="col-md-2">
    <label class="control-label" for="time">End Time: </label>   </div>
   
   <div class="col-md-10">
    <input type="time" name="end_time" id="time" class="form-control" value="{{ date("H:i") }}" required /></div>
   <br>
    <div class="col-md-10">
    <button name="submit" type="submit" value="Save"> save</button></div>
    </form></div>
    @endsection
   





  