@extends('layouts.sidebar')
@section('content')
                
           
<div class="panel panel-default panel-table">
     <div class="row">
             <div class="col col-xs-6" style="font-family: Arial Black, Gadget, sans-serif;">
                <h3>Topics</h3>
             </div>
               
                    
                    <!-- <li><a href="{{route('meets.index')}}" class="btn btn-success">@lang('Back to meets')</a></li> -->
                    
              
 <div class="col col-xs-6 text-right " style="margin-top: 5%;">  
 @cannot('participant')<a href="{{ route('topics.create') }}"
     class="btn btn-sm btn-primary btn-create" >@lang('add a new topic')</a> 
 </div>@endcannot
                </div>
                </div>
            


<div class="table-responsive">
        <table class="table table-bordered">
<thead>
<tr>
<th  style=" text-align: center;"> Name</th>

<th  style=" text-align: center;"> Name_Meet</th>

</tr> 
</thead>
<tbody>
<tr>
@foreach($topics as $topic)

<td align="center" style="vertical-align: middle;" > {{$topic->name}}</td> 
<td align="center" style="vertical-align: middle;"> {{$topic->namemeet}} </td>
@cannot('participant')<td align="center" style="vertical-align: middle;">
<a href="{{route('topics.edit', $topic->id)}}" class="btn btn-success">@lang('Edit')</a>
</td>
<td align="center" style="vertical-align: middle;margin-block-end: 0em;">
<form style="margin-block-end: 0em;" method = 'post' action = "{{action('TopicController@destroy',$topic->id)}}" >
@csrf   
@method('DELETE')   
<div>    

<button type="submit" class="btn btn-danger">@lang('delete')</button>
</div>
</td>@endcannot

</form>

</tr>

@endforeach  
</tbody>
</table>
</div>  
</div>           
            </div>
            


@endsection

<style>
.table-responsive{
    /* padding-top:50px; */
  

}



</style>