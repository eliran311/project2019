@extends('layouts.app')
@section('content')

<h1>Create a new topic</h1>
<form method = 'post' action = "{{action('TopicController@store')}}" >

{{csrf_field()}}      

<div class = "form-group">
<br>
<div class="col-md-4">
    <label class="control-label" for = "name"> topic name </label>
    </div>
    <div class="col-md-10">
    <input type = "text" class = "form-control" name = "name" required>
    </div>
    <br>
  
    @if($meets != null)
    <div class="col-md-4">
    <label class="control-label" for = "name"> Meet Name </label>
    </div>

    <div class="col-md-10">
    <select class="form-control" name = "meet_id">
    @foreach($meets as $meet)
  
  <option value="{{$meet->id}}"> {{$meet->title}}</option>
  @endforeach  

  
</select></div>@endif






    <br>

    <div class = "form-group">    
    <input type = "submit" class = "submit-button" name = "submit" value = "Save">

</div>
</div>

</form>

@endsection


<style>
.submit-button{
  background-color: #555555; 
  border: none;
  color: white;
  padding: 8px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 20px 18px;
  cursor: pointer;
  height: 40px;
}

.form-group{
background: #E6E6FA;
width:50%;
border-radius: 3%;
padding: 2%;
}
</style>
