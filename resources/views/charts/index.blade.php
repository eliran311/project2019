@extends('layouts.sidebar')

@section('content')
<div class="container" style= "    background: #a9a9a9a8;
    border-radius: 2%;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <!-- <div class="panel-heading">Chart Demo</div> -->
                <br>
                @can('manager')    <div class="panel-body"> {!! $chart->html() !!} </div>@endcan('manager')

@cannot('manager')       <div class="panel-body"> {!! $deadline_com->html() !!} </div>@endcannot('manager')

               
                </div>
            </div>
            </div>
                <br>
            <div class="row">
            <div class="col-md-7"> 
            <div style="padding-bottom:2%">{!! $bar_chart->html() !!} </div>
               
               <div>{!! $task_com->html() !!} </div>
            </div>
            
            <div class="col-md-5"> 
           
              <div style="padding:1% 0"> {!! $pie_meet->html() !!}</div>
            </div>
           
            <!-- <div class="col-md-4"> 
               {!! $task_com->html() !!}
               </div> -->

      
    </div>
    <br>
</div>
{!! Charts::scripts() !!}
@can('manager')   {!! $chart->script() !!}@endcan('manager')
{!! $bar_chart->script() !!}
{!! $pie_meet->script() !!}

{!! $task_com->script() !!}
@cannot('manager') {!! $deadline_com->script() !!}@endcannot('manager')

@endsection