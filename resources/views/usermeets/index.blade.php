@extends('layouts.sidebar')

@section('content')


           <div class="panel panel-default">
                <div class="row">
                  <div class="col col-xs-6" style="font-family: Arial Black, Gadget, sans-serif;">
                    <h3 class="panel-title">User Meets</h3>
                  </div>
                  
                  
         <div class="col col-xs-6 text-right"  style="margin-top:50px;">
                        <a href="{{ route('usermeets.create') }}"
          class="btn btn-sm btn-primary btn-create">
      @lang('add a new user_meet')</a> 
         </div>
                </div>
              </div>
<div class="panel-body"style="padding: 1% 0 5% 1%;">
                

<div class="table-responsive">
        <table class="table table-bordered">
      <thead>
        <tr>

          <th style = "vertical-align: middle;text-align: center;border-right: 1px solid #dee2e6;"> namemeet</th>
          <th style = "vertical-align: middle;text-align: center;border-right: 1px solid #dee2e6;"> nameuser</th>
        </tr> 
      </thead>
 <tbody>
                  @foreach($usermeets as $usermeet)


<td align="center"> {{$usermeet->namemeet}} </td>
<td align="center"> {{$usermeet->nameuser}} </td>



@endforeach  
                        

                        </tbody>
                </table>
            
              </div>
            
              </div>
           






@endsection